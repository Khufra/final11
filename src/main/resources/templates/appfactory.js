var app = angular.module('aitu-project', []);

app.controller('FactoryCtrl', function($scope, $http) {

    $scope.signin = {};
    $scope.OrderStatList = [];
    $scope.statusName = [];
    $scope.statuschangelist = {};
    $scope.shop = {};
    $scope.dashboard = [];
    $scope.custom = [];
    $scope.total_price=[];
    $scope.orderLists = {};
    // $scope.shop_id = function(){
    //     $http({
    //         url: 'http://127.0.0.1:8081/order',
    //         method: 'GET',
    //         headers: {
    //             "Access-Control-Allow-Origin": "*",
    //             "Content-Type": "application/json",
    //
    //         },
    //         data : {
    //             'shop_id' : $scope.shop_id(),
    //             'total_price' : $scope.total_price
    //         },
    //
    //     }).then(function (response) {
    //         $scope.custom = response.data;
    //         console.log(response);
    //     }, function (response){
    //         console.log(response);
    //     })
    //
    // }
    $scope.getorderLists = function(shop_id) {
        $http({
            url: 'http://127.0.0.1:8081//orderr/shop',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            },then(response){
                console.log("SUCCESS");

                console.log(response);
                $scope.orderLists = response.data;}
            },function (response) {

            console.log("ERROR");

            console.log(response);
        })


    }
    $scope.getorderLists();
    $scope.total_price = function(){

                angular.forEach($scope.orderLists, function (value) {
                    if($scope.custom === 0){
                        $scope.custom = value;
                    }
                 if($scope.custom.total_price < value.total_price){
                     $scope.custom = value;
                 }
                })



    }
    $scope.total_price();
    $scope.getDashboard = function () {
        $http({
            url: 'http://127.0.0.1:8081/orderItem/dashboard',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            },

        }).then(function (response){
            $scope.dashboard = response.data;
            console.log(response);

        }, function (response){
            console.log(response);
        })
    }

    $scope.getDashboard();

    $scope.signIn = function (){
        $http({
            url: 'http://127.0.0.1:8081/signin/1',
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log("SUCCESS");
            console.log(response);
            $scope.signin = response.data;

        }, function (response){
            console.log("ERROR");
            $scope.signin = {};
            console.log(response);
        })
    };

    $scope.getStatusInfo = function() {
        $http({
            url: 'http://127.0.0.1:8081/status',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log("SUCCESS");
                    console.log(response);
                    $scope.statusName = response.data;
                },
                function (response) { // optional
                    console.log("ERROR");
                    console.log(response);
                });
    };
    $scope.getStatusInfo();

    $scope.getStatus = function(status) {
        $http({
            url: 'http://127.0.0.1:8081/orderrr/'+ status,
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
        })
            .then(function (response) {
                    console.log("SUCCESS");
                    console.log(response);
                    $scope.OrderStatList = response.data;
                },
                function (response) { // optional
                    console.log("ERROR");
                    console.log(response);
                });
    };




    $scope.changeStatus1 = function(order_id,status) {
        if(status<$scope.statusName.length+1){

            $http({
            url: 'http://127.0.0.1:8081/order/update',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "order_id":order_id,
                "status":status
            },

        })
            .then(function (response) {

                    console.log("SUCCESS");
                    console.log(response);
                    location.reload();

                },
                function (response) { // optional
                    console.log("ERROR");
                    console.log(response);
                });
    }};

    $scope.changeStatus = function (order,status){
        if (order.id === undefined) {
            $scope.statuschangelist[order.id] =  {id: order.id, shop_id:order.shop_id,date:order.date,total_price: order.total_price, status: order.status};
        }
        $scope.changeStatus1(order.id,status);
    }

});
