package com.example.demo.Repository;

import com.example.demo.Entity.Factory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FactoryRepository extends CrudRepository<Factory, Long> {

}
