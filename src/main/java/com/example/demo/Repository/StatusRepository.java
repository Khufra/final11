package com.example.demo.Repository;

import com.example.demo.Entity.Shop;
import com.example.demo.Entity.Status;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatusRepository extends CrudRepository<Status, Long> {
    @Query(value = "SELECT name from status",nativeQuery = true)
    List<String> getNames();
    @Query(value = "SELECT MAX(id) FROM status",nativeQuery = true)
   long findByIdMax();

}
