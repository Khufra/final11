package com.example.demo.Service;

import com.example.demo.Entity.Order;
import com.example.demo.Entity.OrderItem;
import com.example.demo.Entity.Product;
import com.example.demo.Entity.Shop;
import com.example.demo.Repository.OrderItemRepository;
import com.example.demo.Repository.OrderRepository;
import com.example.demo.Repository.ProductRepository;
import com.example.demo.Repository.ShopRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;
    private final ShopRepository shopRepository;
    private final ProductRepository productRepository;


    public OrderService(OrderRepository orderRepository, OrderItemRepository orderItemRepository, ShopRepository shopRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.shopRepository = shopRepository;
        this.productRepository = productRepository;
    }

    public List<Order> getAll(){
        return (List<Order>) orderRepository.findAll();
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(orderRepository.findById(id));
    }

    public void delete(long id){
        orderRepository.deleteById(id);
    }
    public Order update(@RequestBody Order order){
        return  orderRepository.save(order);
    }
    public void save(long shop_id){
        LocalDate date = LocalDate.now();
        float total_price=0;
        orderRepository.insertorder(shop_id,total_price,date,1);
    }
    public ResponseEntity<?> getByShop_id(long id) {
        return ResponseEntity.ok(orderRepository.findByShopId(id));
    }
    public List<Order> getByStatus(long status) {
        return orderRepository.getByStatus(status);
    }
    public ResponseEntity<?> sumByShopId(){return ResponseEntity.ok(orderRepository.sumByShopId());}

    public void changeStatus(long id, long status) {
        orderRepository.updateStatus(status,id);
    }

/*
    @Transactional
    public List<Object> getDashboard(){
        List<Order> orders = (List<Order>) orderRepository.findAll();
        for(Order order: orders) {
            order.setOrderItems(orderItemRepository.findByOrder_id(order.getId()));
            order.setShop((List<Shop>) shopRepository.findById(order.getShop_id()));
            List<Optional<Product>> productList = new ArrayList<>();
            for (OrderItem products: order.getOrderItems()) {
                Optional<Product> product = productRepository.findById(products.getProduct_id());
                productList.add(product);
            }
            order.setProducts(productList);
        }

        return orders;
    }*/

}
