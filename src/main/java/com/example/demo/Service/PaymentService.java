package com.example.demo.Service;

import com.example.demo.Entity.Payment;
import com.example.demo.Entity.Product;
import com.example.demo.Repository.PaymentRepository;
import com.example.demo.Repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentService {
    private final PaymentRepository paymentRepository;

    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(paymentRepository.findAll());
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(paymentRepository.findById(id));
    }

    public void delete(long id){
        paymentRepository.deleteById(id);
    }
    public Payment update(@RequestBody Payment payment){
        return  paymentRepository.save(payment);
    }

}
