package com.example.demo.Controller;

import com.example.demo.Entity.Order;
import com.example.demo.Entity.OrderItem;
import com.example.demo.Entity.Shop;
import com.example.demo.Service.OrderService;
import com.example.demo.Service.ShopService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(value="/order",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(orderService.getAll());

    }
    @RequestMapping(value="/order/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(orderService.getById(id));}
    @RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        orderService.delete(id);
    }
    @RequestMapping(value="/order",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Order order){
        return ResponseEntity.ok(orderService.update(order));
    }
    @PostMapping(value="/ordder")
    public void save(@RequestHeader("shop_id") long shop_id){
         orderService.save(shop_id);
    }
    @GetMapping(value = "/orderr/shop")
    public ResponseEntity<?> sumByShopId(){
        return ResponseEntity.ok(orderService.sumByShopId());
    }
    @RequestMapping(value="/order/shop/{shop_id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getshopid(@PathVariable long shop_id){
        return ResponseEntity.ok(orderService.getByShop_id(shop_id));}
    @RequestMapping(value="/orderrr/{status}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getStatus(@PathVariable long status){
        return ResponseEntity.ok(orderService.getByStatus(status));}
    @RequestMapping(value="/order/update", method=RequestMethod.POST, headers = "Accept=application/json")
    public void ChangeStatus(@RequestHeader("order_id") Long order_id, @RequestHeader("status") Long status){
        orderService.changeStatus(order_id,status);
    }
   /* @RequestMapping(value="/order/dashboard", method=RequestMethod.GET, headers = "Accept=application/json")
    public  List<Object> getDashboard(){
       return orderService.getDashboard();
    }*/
}
