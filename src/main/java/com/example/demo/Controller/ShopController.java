package com.example.demo.Controller;

import com.example.demo.Entity.Product;
import com.example.demo.Entity.Shop;
import com.example.demo.Service.ProductService;
import com.example.demo.Service.ShopService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShopController {
    private final ShopService shopService;

    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @RequestMapping(value="/shop",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(shopService.getAll());

    }

    @RequestMapping(value="/shop/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(shopService.getById(id));}
    @RequestMapping(value = "/shop/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable    long id){
        shopService.delete(id);
    }
    @RequestMapping(value="/shop",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Shop shop){
        return ResponseEntity.ok(shopService.update(shop));
    }
    @RequestMapping(value="/customers/me",method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getMe(@RequestHeader("auth") String token){
        try{
            return ResponseEntity.ok(shopService.getCustomerByToken(token));
        }
catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
}
    }

}
