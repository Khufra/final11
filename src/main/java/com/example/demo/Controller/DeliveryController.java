package com.example.demo.Controller;

import com.example.demo.Entity.Delivery;
import com.example.demo.Entity.Factory;
import com.example.demo.Service.DeliveryService;
import com.example.demo.Service.FactoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeliveryController {
    private final DeliveryService deliveryService;

    public DeliveryController(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @RequestMapping(value="/delivery",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(deliveryService.getAll());

    }
    @RequestMapping(value="/delivery/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(deliveryService.getById(id));}
    @RequestMapping(value = "/delivery/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        deliveryService.delete(id);
    }
    @RequestMapping(value="/delivery",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Delivery delivery){
        return ResponseEntity.ok(deliveryService.update(delivery));
    }
}
