package com.example.demo.Controller;

import com.example.demo.Entity.Driver;
import com.example.demo.Service.DriverService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DriverController {
    private final DriverService driverService;

    public DriverController(DriverService driverService) {
        this.driverService = driverService;
    }

    @RequestMapping(value="/driver",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(driverService.getAll());

    }
    @RequestMapping(value="/driver/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(driverService.getById(id));}
    @RequestMapping(value = "/driver/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        driverService.delete(id);
    }
    @RequestMapping(value="/driver",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Driver driver){
        return ResponseEntity.ok(driverService.update(driver));
    }
}
